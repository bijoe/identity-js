var gulp = require('gulp');
var concat = require('gulp-concat');
var ngAnnotate = require('gulp-ng-annotate');

gulp.task('default', function () {
  return gulp.src(['src/module.js', 'src/**/*.js'])
    .pipe(concat('identity.js'))
    .pipe(ngAnnotate())
    .pipe(gulp.dest('dist'));
});
