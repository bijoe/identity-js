angular.module('identity.js', ['ui.router', 'ngCookies']);

angular.module('identity.js').run(/* @ngInject */ ["$rootScope", "$state", "IdentityJS", "IdentityJSConfig", "IdentityReferer", function ($rootScope, $state, IdentityJS, IdentityJSConfig, IdentityReferer) {
  $rootScope.$on('$stateChangeStart', function (event, nextState, nextParams) {
    var security = nextState.security === undefined || nextState.security;
    if (!IdentityJS.authorize(security)) {
      event.preventDefault();
      if (IdentityJS.isLoggedIn()) {
        $state.transitionTo(IdentityJSConfig.unauthorizedState);
      } else {
        IdentityReferer.set({ state: nextState.name, params: nextParams });
        $state.transitionTo(IdentityJSConfig.loginState);
      }
    }
  });
}]);

function IdentityJSConfigProvider() {

  var config = {
    host: '/identity',
    loginUrl: 'login',
    logoutUrl: 'logout',
    loggedinUrl: 'me',

    unauthorizedState: 'unauthorized',
    targetState: 'home',
    loginState: 'login',
    tokenName: 'authtoken'
  };

  this.setConfig = function (configuration) {
    config = configuration;
  };

  this.$get = /* @ngInject */ function () {
    return {
      host: config.host,
      loginUrl: config.loginUrl,
      logoutUrl: config.logoutUrl,
      loggedinUrl: config.loggedinUrl,

      targetState: config.targetState,
      loginState: config.loginState,
      unauthorizedState: config.unauthorizedState,
      tokenName: config.tokenName,
    };
  };
}

angular
  .module('identity.js')
  .provider('IdentityJSConfig', IdentityJSConfigProvider);

function IdentityLoginFormDirective() {
  return {
    scope: {
      account: '=account'
    },
    controller: /* @ngInject */ ["$scope", "$state", "IdentityJS", "IdentityJSConfig", "IdentityReferer", function ($scope, $state, IdentityJS, IdentityJSConfig, IdentityReferer) {
      $scope.error    = false;
      $scope.ready  = false;

      var redirect = function () {
        if (IdentityReferer.has()) {
          var referer = IdentityReferer.get();
          IdentityReferer.reset();
          $state.transitionTo(referer.state, referer.params);
        } else {
          $state.transitionTo(IdentityJSConfig.targetState);
        }
      };

      // Check Login
      IdentityJS.check().then(function() {
        redirect();
      }, function() {
        $scope.ready = true;
      });

      // Login
      $scope.submit = function() {
        if (!$scope.loading) {
          $scope.loading  = true;
          IdentityJS.login($scope.username, $scope.password, $scope.account).then(function() {
            $scope.loading  = false;
            redirect();
          }, function() {
            $scope.loading  = false;
            $scope.error = true;
          });
        }
      };
    }],

    templateUrl: function(element, attr) {
      return attr.templateUrl ? attr.templateUrl : 'identityJS/login.html';
    }
  };
}

angular
  .module('identity.js')
  .directive('identityLoginForm', IdentityLoginFormDirective);

angular.module('identity.js').run(/* @ngInject */ ["$templateCache", function($templateCache) {
  $templateCache.put('identityJS/login.html',
    '<form class="login-form" data-ng-show="ready" data-ng-submit="submit()">' +
        '<div class="alert alert-error" ng-show="error">' +
            'Check your username or password' +
            '<button class="close" ng-click="error = false">&times;</button>' +
        '</div>' +
        '<input type="text" placeholder="Username" data-ng-model="username" />' +
        '<input type="password" placeholder="Password" data-ng-model="password" />' +
        '<input type="submit" value="Login" />'+
    '</form>'
  );
}]);

/* @ngInject */
function IdentityHttpInterceptor($httpProvider) {
    // alternatively, register the interceptor via an anonymous factory
    $httpProvider.interceptors.push(/* @ngInject */ ["$cookies", "IdentityJSConfig", function($cookies, IdentityJSConfig) {
      return {
        request: function(config) {
          var token = $cookies[IdentityJSConfig.tokenName];
          if (token) {
            if (!config.headers) {
              config.headers = {};
            }
            config.headers[IdentityJSConfig.tokenName] = token;
          }

          return config;
        }
      };
  }]);
}
IdentityHttpInterceptor.$inject = ["$httpProvider"];

angular
  .module('identity.js')
  .config(IdentityHttpInterceptor);

/* @ngInject */
function IdentityReferer() {

  return {
    referer: false,

    has: function() {
      return this.referer !== false;
    },

    reset: function () {
      this.referer = false;
    },

    set: function (referer) {
      this.referer = referer;
    },

    get: function () {
      return this.referer;
    }
  };

}

angular
  .module('identity.js')
  .factory('IdentityReferer', IdentityReferer);

/* @ngInject */
function IdentityJS ($q, $http, $rootScope, $cookies, IdentityJSConfig) {

  var user = null,
      lastUser = null;

  return {

    getUser: function () {
      return user;
    },

    getLastUser: function () {
      return lastUser;
    },

    isLoggedIn: function () {
      return user !== null;
    },

    authorize: function (feature) {
      return !feature || (
        user !== null &&
          (feature === true || user.features.indexOf(feature) !== -1)
        );
    },

    login: function (username, password, account) {
      return $http.post(IdentityJSConfig.host + IdentityJSConfig.loginUrl, {
          username: username,
          password: password,
          account: account
        }).then(function (response) {
          $cookies[IdentityJSConfig.tokenName] = response.data.token;

          return this.check();
        }.bind(this));
    },

    logout: function () {
      return $http.get(IdentityJSConfig.host + IdentityJSConfig.logoutUrl).then(function () {
        $cookies[IdentityJSConfig.tokenName] = null;
        user = null;
        $rootScope.$broadcast('IdentityJS.logout');
        return 'ok';
      });
    },

    check: function () {
      var defer = $q.defer();
      $http.get(IdentityJSConfig.host + IdentityJSConfig.loggedinUrl).then(function (response) {
        var previous = user;
        user = response.data;
        lastUser = response.data;
        if (!angular.equals(previous, user)) {
          $rootScope.$broadcast('IdentityJS.login', user);
        }

        defer.resolve(user);
      }, function () {
        if (user !== null) {
            $cookies[IdentityJSConfig.tokenName] = null;
            $rootScope.$broadcast('IdentityJS.logout');
        }
        user = null;
        defer.reject();
      });

      return defer.promise;
    }
  };
}
IdentityJS.$inject = ["$q", "$http", "$rootScope", "$cookies", "IdentityJSConfig"];

angular
  .module('identity.js')
  .factory('IdentityJS', IdentityJS);
