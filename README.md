Identity
========

Identity Angular Integration

Installation
------------

To use this library you should have a token base authentication server like [identity](https://github.com/BI-joe/identity)

 * Install the library

```bash
bower install identityJS
```

 * load the library

```html
<script src="js/angular.min.js"></script>
<script src="js/angular-route.min.js"></script>
<script src="js/identity.js"></script>
```

 * add it to your dependencies

```javascript
angular.module('myapp', ['identity.js']);
```

 * configure

```javascript
angular.module('myapp').config(['IdentityJSProvider', function (IdentityJSConfigProvider) {

    IdentityJSConfigProvider.setConfig({
        host: 'api/',                  // your base api url
        loginUrl: 'auth/login',        // login api url
        logoutUrl: 'auth/logout',      // logout api url
        loggedinUrl: 'auth/loggedin',  // api to get the user profile and roles

        unauthorizedState: '/unauthorized',  // ui state (frontend) of the unauthorized page
        targetState: '/dashboard',           // ui state (frontend) of the target page on login success
        loginState: '/login',                // ui state (frontend) of the login page
        tokenName: 'authtoken'              // the http header name for the token
    });

}]);
```

Usage
-----

 * In your login page, include the login form like this

```html
<div identity-login-form></div>
```
You can override the default login form template like this

```html
<div identity-login-form template-url="mypartial.html"></div>
```

 * add a security attribute to your routes
     * a false value means that the route is not protected,
     * a true value means, you have to be loggedin to access this route,
     * other custom string can be used to indicate that a user role is required to access this route (the string represent the role that have to be found in user.roles)

* you can call IdentityJS.logout(); to loggout

* you cas use IdentityJS.getLoggedinUser() to get the current loggedin user
