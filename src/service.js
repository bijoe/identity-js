/* @ngInject */
function IdentityJS ($q, $http, $rootScope, $cookies, IdentityJSConfig) {

  var user = null,
      lastUser = null;

  return {

    getUser: function () {
      return user;
    },

    getLastUser: function () {
      return lastUser;
    },

    isLoggedIn: function () {
      return user !== null;
    },

    authorize: function (feature) {
      return !feature || (
        user !== null &&
          (feature === true || user.features.indexOf(feature) !== -1)
        );
    },

    login: function (username, password, account) {
      return $http.post(IdentityJSConfig.host + IdentityJSConfig.loginUrl, {
          username: username,
          password: password,
          account: account
        }).then(function (response) {
          $cookies[IdentityJSConfig.tokenName] = response.data.token;

          return this.check();
        }.bind(this));
    },

    logout: function () {
      return $http.get(IdentityJSConfig.host + IdentityJSConfig.logoutUrl).then(function () {
        $cookies[IdentityJSConfig.tokenName] = null;
        user = null;
        $rootScope.$broadcast('IdentityJS.logout');
        return 'ok';
      });
    },

    check: function () {
      var defer = $q.defer();
      $http.get(IdentityJSConfig.host + IdentityJSConfig.loggedinUrl).then(function (response) {
        var previous = user;
        user = response.data;
        lastUser = response.data;
        if (!angular.equals(previous, user)) {
          $rootScope.$broadcast('IdentityJS.login', user);
        }

        defer.resolve(user);
      }, function () {
        if (user !== null) {
            $cookies[IdentityJSConfig.tokenName] = null;
            $rootScope.$broadcast('IdentityJS.logout');
        }
        user = null;
        defer.reject();
      });

      return defer.promise;
    }
  };
}

angular
  .module('identity.js')
  .factory('IdentityJS', IdentityJS);
