/* @ngInject */
function IdentityReferer() {

  return {
    referer: false,

    has: function() {
      return this.referer !== false;
    },

    reset: function () {
      this.referer = false;
    },

    set: function (referer) {
      this.referer = referer;
    },

    get: function () {
      return this.referer;
    }
  };

}

angular
  .module('identity.js')
  .factory('IdentityReferer', IdentityReferer);
