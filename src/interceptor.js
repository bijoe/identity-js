/* @ngInject */
function IdentityHttpInterceptor($httpProvider) {
    // alternatively, register the interceptor via an anonymous factory
    $httpProvider.interceptors.push(/* @ngInject */ function($cookies, IdentityJSConfig) {
      return {
        request: function(config) {
          var token = $cookies[IdentityJSConfig.tokenName];
          if (token) {
            if (!config.headers) {
              config.headers = {};
            }
            config.headers[IdentityJSConfig.tokenName] = token;
          }

          return config;
        }
      };
  });
}

angular
  .module('identity.js')
  .config(IdentityHttpInterceptor);
