angular.module('identity.js', ['ui.router', 'ngCookies']);

angular.module('identity.js').run(/* @ngInject */ function ($rootScope, $state, IdentityJS, IdentityJSConfig, IdentityReferer) {
  $rootScope.$on('$stateChangeStart', function (event, nextState, nextParams) {
    var security = nextState.security === undefined || nextState.security;
    if (!IdentityJS.authorize(security)) {
      event.preventDefault();
      if (IdentityJS.isLoggedIn()) {
        $state.transitionTo(IdentityJSConfig.unauthorizedState);
      } else {
        IdentityReferer.set({ state: nextState.name, params: nextParams });
        $state.transitionTo(IdentityJSConfig.loginState);
      }
    }
  });
});
