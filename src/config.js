function IdentityJSConfigProvider() {

  var config = {
    host: '/identity',
    loginUrl: 'login',
    logoutUrl: 'logout',
    loggedinUrl: 'me',

    unauthorizedState: 'unauthorized',
    targetState: 'home',
    loginState: 'login',
    tokenName: 'authtoken'
  };

  this.setConfig = function (configuration) {
    config = configuration;
  };

  this.$get = /* @ngInject */ function () {
    return {
      host: config.host,
      loginUrl: config.loginUrl,
      logoutUrl: config.logoutUrl,
      loggedinUrl: config.loggedinUrl,

      targetState: config.targetState,
      loginState: config.loginState,
      unauthorizedState: config.unauthorizedState,
      tokenName: config.tokenName,
    };
  };
}

angular
  .module('identity.js')
  .provider('IdentityJSConfig', IdentityJSConfigProvider);
