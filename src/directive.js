function IdentityLoginFormDirective() {
  return {
    scope: {
      account: '=account'
    },
    controller: /* @ngInject */ function ($scope, $state, IdentityJS, IdentityJSConfig, IdentityReferer) {
      $scope.error    = false;
      $scope.ready  = false;

      var redirect = function () {
        if (IdentityReferer.has()) {
          var referer = IdentityReferer.get();
          IdentityReferer.reset();
          $state.transitionTo(referer.state, referer.params);
        } else {
          $state.transitionTo(IdentityJSConfig.targetState);
        }
      };

      // Check Login
      IdentityJS.check().then(function() {
        redirect();
      }, function() {
        $scope.ready = true;
      });

      // Login
      $scope.submit = function() {
        if (!$scope.loading) {
          $scope.loading  = true;
          IdentityJS.login($scope.username, $scope.password, $scope.account).then(function() {
            $scope.loading  = false;
            redirect();
          }, function() {
            $scope.loading  = false;
            $scope.error = true;
          });
        }
      };
    },

    templateUrl: function(element, attr) {
      return attr.templateUrl ? attr.templateUrl : 'identityJS/login.html';
    }
  };
}

angular
  .module('identity.js')
  .directive('identityLoginForm', IdentityLoginFormDirective);

angular.module('identity.js').run(/* @ngInject */ function($templateCache) {
  $templateCache.put('identityJS/login.html',
    '<form class="login-form" data-ng-show="ready" data-ng-submit="submit()">' +
        '<div class="alert alert-error" ng-show="error">' +
            'Check your username or password' +
            '<button class="close" ng-click="error = false">&times;</button>' +
        '</div>' +
        '<input type="text" placeholder="Username" data-ng-model="username" />' +
        '<input type="password" placeholder="Password" data-ng-model="password" />' +
        '<input type="submit" value="Login" />'+
    '</form>'
  );
});
